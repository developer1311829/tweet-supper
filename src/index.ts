import databaseService from '@/services/database.service'
import express from 'express'
import { defaultErrorHandler } from './middlewares/error.middlewares'
import { routes } from './routes'

export const app = express()
const port = 2503
databaseService.connect()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

routes(app)
console.log(routes)
app.use(defaultErrorHandler)
app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
